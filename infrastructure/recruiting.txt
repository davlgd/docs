- Have at least two core devs suggest them in #exherbo-dev and discuss
- Talk to them, ask if they're interested
- Create an account on dev.exherbo.org, set initial password, add ssh public
  key
- Add public key as <name>.pub to keydir and add their name to @dev in
  conf/gitolite.conf in gitolite-admin.git
- Invite them to Exherbo, Exherbo Dev, Exherbo Unofficial and Exherbo Misc
  groups on gitlab
- Invite them to the Exherbo organisation on Github
- Ask in #libera-communities for a exherbo/developer cloak for their account
  (probably needs kloeri or heirecka because they are group contacts)
- Invite them to #exherbo-dev
- Create a dev repo if wanted (also in gitolite-admin.git)

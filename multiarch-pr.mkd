Title: Multiarch Press Release
CSS: /css/main.css

#{include head}

Multiple Architecture support in Exherbo
=========

As of the beginning of this month, Exherbo Linux introduced additional concepts
to Linux package management, which allow cross-compiling to different architectures
while still being able to use the features and dependency resolution of the package
manager more easily than any other available distro.

On most distributions, cross-compilation is often handled by special-purpose tools,
entirely separate from the distribution's package manager. This presents a number
of difficulties, including having to deal with how building a package for the target
architecture may require additional tools on the host system – an obstacle that
must be resolved manually. And, while the tool can resolve dependencies on the target
and the package manager can do the same for the host, neither can interoperate with
the other.

Exherbo’s new multi-arch strategy instead adds cross-compilation as a built-in
feature of the package manager, enabling seamless handling of such issues. This
permits a single host system to compile packages for any supported target
architecture (which can also be incompatible), without requiring the user to provide
manual intervention or custom-built toolchains.

Most distributions provide similar, but limited, functionality through a split
between the 32-bit and 64-bit software on the system, often under the name "multilib".
However, this only addresses the much narrower problem of allowing 64-bit systems
to run 32-bit software. Exherbo originally did the same, but multi-arch provides
a more flexible solution: in separating the filesystem structure by the target
architecture, it permits far more than a simple lib64 vs. lib32 split. With each
architecture able to coexist in the filesystem, multilib-like behavior can be
supported in an architecture-agnostic way.

These multi-arch features have resulted in other enhancements to the source-based
Exheres package format as well, such as the ability to seperate different parts
of the package from each other, combining some perks of binary-based package
management with the flexibility of source-based package management. Akin to how
binary distributions often provide development and library packages, you can now
install only certain parts of a package if you desire, rather than everything.
Exherbo sidesteps the issues inherent in having wholly separate packages for
development headers, docs, and other components by expressing such parts as options
on a single package.

Multi-arch has been worked on for about three years now and the final merge was
prepared over the last month. Work continues towards converting packages to take full
advantage of the new technology, and all contributions are welcome.

--
Copyright 2015 Kylie McClain

#{include CC_3.0_Attribution}
#{include foot}

<!-- vim: set et ft=mkd spell spelllang=en sts=4 sw=4 ts=8 : -->
